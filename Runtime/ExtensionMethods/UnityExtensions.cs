﻿namespace UnityEngine
{
    public static class UnityExtensions
    {
        public static T GetComponentInHierarchy<T>(this Component component, 
            bool includeInactive = false) where T : Component
        {
            return component.transform.root.GetComponentInChildren<T>(includeInactive);
        }

        public static T[] GetComponentsInHierarchy<T>(this Component component,
            bool includeInactive) where T : Component
        {
            return component.transform.root.GetComponentsInChildren<T>(includeInactive);
        }
    
        public static T GetComponentInHierarchy<T>(this GameObject gameObject, 
            bool includeInactive = false) where T : Component
        {
            return gameObject.transform.root.GetComponentInChildren<T>(includeInactive);
        }

        public static T[] GetComponentsInHierarchy<T>(this GameObject gameObject,
            bool includeInactive) where T : Component
        {
            return gameObject.transform.root.GetComponentsInChildren<T>(includeInactive);
        }
        
        public static Vector3 Clamp(this Vector3 vector, float min, float max)
        {
            return new Vector3(Mathf.Clamp(vector.x, min, max),
                Mathf.Clamp(vector.y, min, max),
                Mathf.Clamp(vector.z, min, max));
        }
    
        public static Vector2 Clamp(this Vector2 vector, float min, float max)
        {
            return new Vector3(Mathf.Clamp(vector.x, min, max),
                Mathf.Clamp(vector.y, min, max));
        }
    
        public static Vector3 Clamp01(this Vector3 vector)
        {
            return new Vector3(Mathf.Clamp01(vector.x),
                Mathf.Clamp01(vector.y),
                Mathf.Clamp01(vector.z));
        }
    
        public static Vector2 Clamp01(this Vector2 vector)
        {
            return new Vector3(Mathf.Clamp01(vector.x),
                Mathf.Clamp01(vector.y));
        }

        public static Vector3 NewX(this Vector3 vector, float x)
        {
            return new Vector3(x, vector.y, vector.z);
        }
    
        public static Vector3 NewY(this Vector3 vector, float y)
        {
            return new Vector3(vector.x, y, vector.z);
        }
    
        public static Vector3 NewZ(this Vector3 vector, float z)
        {
            return new Vector3(vector.x, vector.y, z);
        }
    
        public static Vector2 NewX(this Vector2 vector, float x)
        {
            return new Vector2(x, vector.y);
        }
    
        public static Vector2 NewY(this Vector2 vector, float y)
        {
            return new Vector2(vector.x, y);
        }
    }
}