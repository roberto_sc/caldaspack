﻿using UnityEngine;

namespace CaldasPack.Prototyping
{
	/// <summary>
	/// Based on https://grabthiscode.com/csharp/fly-cam-extended-script-unity-3d
	/// </summary>
	public class FirstPersonCameraController : MonoBehaviour
	{


		[SerializeField] private float rotationSpeed = 200;
		[SerializeField] private float climbSpeed = 4;
		[SerializeField] private float normalMoveSpeed = 20;
		[SerializeField] private float slowMoveFactor = 0.25f;
		[SerializeField] private float fastMoveFactor = 3;

		private float rotationX = 0.0f;
		private float rotationY = 0.0f;

		private void Update()
		{
			if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
			{
				Cursor.visible = true;
				return;
			}

			Cursor.visible = false;

			float speed = normalMoveSpeed;

			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			{
				speed *= fastMoveFactor;
			}
			else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
			{
				speed *= slowMoveFactor;
			}

			rotationX += Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
			rotationY += Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime;
			rotationY = Mathf.Clamp(rotationY, -90, 90);

			var rotation = Quaternion.AngleAxis(rotationX, Vector3.up);
			rotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
			transform.localRotation = rotation;

			transform.position += Input.GetAxis("Vertical") * speed * Time.deltaTime * transform.forward;
			transform.position += Input.GetAxis("Horizontal") * speed * Time.deltaTime * transform.right;


			if (Input.GetKey(KeyCode.Q))
			{
				transform.position += climbSpeed * Time.deltaTime * transform.up;
			}

			if (Input.GetKey(KeyCode.E))
			{
				transform.position -= climbSpeed * Time.deltaTime * transform.up;
			}

		}
	}
}