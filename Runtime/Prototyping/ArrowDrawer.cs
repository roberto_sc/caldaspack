﻿using UnityEngine;
 
namespace CaldasPack.Prototyping
{ 
    public static class ArrowDrawer
    {
        public static void DrawGizmo (Vector3 position, Vector3 direction,
            float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            if (direction.sqrMagnitude < 0.001f)
            {
                return;
            }
            
            Gizmos.DrawRay (position, direction);
            
            var (h1, h2, h3, h4) = CalculateArrowHead(direction, arrowHeadAngle);
            Gizmos.DrawRay (position + direction, h1 * arrowHeadLength);
            Gizmos.DrawRay (position + direction, h2 * arrowHeadLength);
            Gizmos.DrawRay (position + direction, h3 * arrowHeadLength);
            Gizmos.DrawRay (position + direction, h4 * arrowHeadLength); 
        }

        public static void DrawDebug (Vector3 pos, Vector3 direction, Color? color = null, 
            float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            if (direction.sqrMagnitude < 0.001f)
            {
                return;
            }

            var c = color ?? Color.white;
            
            Debug.DrawRay (pos, direction, c);
            
            var (h1, h2, h3, h4) = CalculateArrowHead(direction, arrowHeadAngle);
            Debug.DrawRay (pos + direction, h1 * arrowHeadLength, c);
            Debug.DrawRay (pos + direction, h2 * arrowHeadLength, c);
            Debug.DrawRay (pos + direction, h3 * arrowHeadLength, c);
            Debug.DrawRay (pos + direction, h4 * arrowHeadLength, c);
        }
       
        private static (Vector3, Vector3, Vector3, Vector3) CalculateArrowHead (Vector3 direction, float arrowHeadAngle)
        {
            var h1 = Quaternion.LookRotation (direction) * Quaternion.Euler (arrowHeadAngle, 0, 0) * Vector3.back;
            var h2 = Quaternion.LookRotation (direction) * Quaternion.Euler (-arrowHeadAngle, 0, 0) * Vector3.back;
            var h3 = Quaternion.LookRotation (direction) * Quaternion.Euler (0, arrowHeadAngle, 0) * Vector3.back;
            var h4 = Quaternion.LookRotation (direction) * Quaternion.Euler (0, -arrowHeadAngle, 0) * Vector3.back;

            return (h1, h2, h3, h4);
        }
    }
}