﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace CaldasPack.Prototyping
{
    /// <summary>
    /// Renderer that can be used in Play or Edit mode to draw gizmos.
    /// Can be called from any method, but is only rendered for 1 frame.
    /// Loads itself into memory when called and creates a hidden game object
    /// so the scene can never be saved with it.
    /// </summary>
    public class StringRenderer : MonoBehaviour
    {
        /// <summary>
        /// Adjust here how long you want the string to be rendered for:
        /// </summary>
        private const double StringLifeTime = 0.016d; // ~1 frame 
        
        private readonly List<GizmoString> strings = new();
        private static StringRenderer instance;

        [Conditional("UNITY_EDITOR")]
        public static void Draw(string text, Vector3 position, float offsetX = 0, float offsetY = 0)
        {
            Draw(text, Color.black, position, offsetX, offsetY);
        }


        [Conditional("UNITY_EDITOR")]
        public static void Draw(string text, Color color, Vector3 position, float offsetX = 0, float offsetY = 0)
        {
            if (instance == null)
            {
                // TODO for some reason this and other methods never find GOs with HideFlags.DontSave
                // I'm leaving it here as a reminder that it would be good to delete old renderers. 
                foreach (var oldRenderer in FindObjectsByType<StringRenderer>(FindObjectsInactive.Include, FindObjectsSortMode.None))
                {
                    DestroyImmediate(oldRenderer.gameObject);
                }

                var go = new GameObject(nameof(StringRenderer));

                instance = go.AddComponent<StringRenderer>();
                if (EditorApplication.isPlaying)
                {
                    DontDestroyOnLoad(go);
                }
            }

            instance.strings.Add(new()
            {
                ExpiresAt = EditorApplication.timeSinceStartup + StringLifeTime,
                Text = text,
                Position = position,
                Style = new() { normal = { textColor = color } },
                Offset = new(offsetX, offsetY)
            });

            GUIStyle style = new() { normal = { textColor = color } };
        }


        private void OnDrawGizmos()
        {
            Handles.BeginGUI();
            foreach (var str in strings)
            {
                DrawString(str);
            }
            Handles.EndGUI();
            strings.RemoveAll(str => str.ExpiresAt <= EditorApplication.timeSinceStartup);
        }

        private void DrawString(GizmoString str)
        {
            var cam = Camera.current;
            var position = cam.ScreenToWorldPoint(cam.WorldToScreenPoint(str.Position) + str.Offset);
            Handles.Label(position, str.Text, str.Style);
        }

        private struct GizmoString
        {
            public string Text;
            public GUIStyle Style;
            public Vector3 Position;
            public Vector3 Offset;
            public double ExpiresAt;
        }
    }
}

