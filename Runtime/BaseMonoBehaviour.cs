﻿using System;
using System.Collections;
using UnityEngine;

namespace CaldasPack
{
    public class BaseMonoBehaviour : MonoBehaviour
    {
        public Coroutine Delay(float delay, Action action)
        {
            return StartCoroutine(DoDelay(delay, action));
        }

        public Coroutine Delay<T>(float delay, Action<T> action, T t)
        {
            return StartCoroutine(DoDelay(delay, action, t));
        }

        public Coroutine Loop(float delay, Action action)
        {
            return StartCoroutine(DoLoop(delay, action));
        }

        public Coroutine Loop<T>(float delay, Action<T> action, T t)
        {
            return StartCoroutine(DoLoop(delay, action, t));
        }

        private static IEnumerator DoDelay(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            action();
        }

        private static IEnumerator DoDelay<T>(float delay, Action<T> action, T t)
        {
            yield return new WaitForSeconds(delay);
            action(t);
        }

        private static IEnumerator DoLoop(float delay, Action action)
        {
            var wait = new WaitForSeconds(delay);
            while (true)
            {
                action();
                yield return wait;
            }
        }

        private static IEnumerator DoLoop<T>(float delay, Action<T> action, T t)
        {
            var wait = new WaitForSeconds(delay);
            while (true)
            {
                action(t);
                yield return wait;
            }
        }
    }
}
