﻿using UnityEngine;
using Random = UnityEngine.Random;
using C = UnityEngine.Color;

namespace CaldasPack
{
    /// <summary>
    /// This file can hopefully grow to a number of small classes with helper methods.
    /// </summary>
    public static class RandomHelper
    {
        private static Color[] colors;
        public static bool Bool()
        {
            return Random.value > 0.5f;
        }

        public static Color Color()
        {
            if (colors == null)
            {
                colors = new[] {C.black, C.blue, C.cyan, C.gray, C.green, C.magenta, C.red, C.white, C.yellow};
            }

            return colors.RandomElement();
        }
    }
}
