﻿using System.Text;

namespace CaldasPack
{
    public class CircularBuffer<T>
    {
        private readonly T[] buffer;
        private readonly int size;
        private bool overflow;
        private int index;

        public int Count => overflow ? size : index;

        public CircularBuffer(int size)
        {
            buffer = new T[size];
            this.size = size;
        }

        public void Add(T t)
        {
            buffer[index] = t;
            index++;
            if (index == size)
                overflow = true;
            index %= size;
        }

        public T this[int i] => overflow ? buffer[(index + i) % size] : buffer[i];

        public T[] ToArray()
        {
            T[] array = new T[size];
            for (int i = 0; i < Count; i++)
            {
                array[i] = this[i];
            }

            return array;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Count; i++)
            {
                sb.Append(this[i]);
                sb.Append('\n');
            }

            return sb.ToString();
        }
    }
}